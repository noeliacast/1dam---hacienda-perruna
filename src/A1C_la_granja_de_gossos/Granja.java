package A1C_la_granja_de_gossos;

import java.io.RandomAccessFile;
import java.util.Random;

public class Granja {

	private Gos[] gossos;
	private int numGossos;
	private int topGossos;
	private static Raca[] races = { new Raca("Pastor alemany", GosMida.GRAN, 13), new Raca("Pitbull", GosMida.MITJA, 8),
			new Raca("Caniche", GosMida.PETIT, 10), new Raca("Dalmata", GosMida.GRAN, 9),
			new Raca("Labrador", GosMida.GRAN, 14), new Raca("Mastin", GosMida.GRAN, 11),
			new Raca("San bernardo", GosMida.GRAN, 10), new Raca("Pug", GosMida.PETIT, 15),
			new Raca("Bulldog", GosMida.PETIT, 10)

	};

	public Granja() {
		// constructor
		topGossos = 100;
		numGossos = 0;
		gossos = new Gos[topGossos];
	}

	public Granja(int top) {
		// constructor modificar top gossos
		numGossos = 0;
		if (top >= 1 && top <= 100) {
			this.topGossos = top;
		} else {
			topGossos = 100;
		}
		gossos = new Gos[topGossos];
	}

	public Gos[] getGos() {

		return gossos;
	}

	public int getnumGossos() {
		return numGossos;

	}

	public int gettopGossos() {
		return topGossos;
	}

	public int afegir(Gos g) {
		if (numGossos < topGossos) {
			gossos[numGossos] = g;
			numGossos++;
			return numGossos;
		} else {
			System.out.println("No hi ha espai pel gos!");
			return -1;
		}

	}

	@Override
	public String toString() {
		int i = 0;
		String string = "Numero de gossos: " + numGossos + "\nLimit de gossos: " + topGossos + "\n";
		System.out.println("Gossos: ");
		while (i < numGossos) {
			string = string + gossos[i].toString();
			System.out.println();
			i++;
		}

		return string;
	}

	public void visualitzar() {

		System.out.println(toString());
	}

	public void visualitzarVius() {
		int i = 0;
		while (i < numGossos) {
			if (gossos[i].getEstat() == GosEstat.VIU) {
				System.out.println(gossos[i]);
			}
			i++;
		}
	}

	static Granja generarGranja() {
		Random rand = new Random();
		Granja granja = new Granja(100);
		int i = 0;
		int mf = 0;
		char sexe = ' ';

		while (i < granja.topGossos) {
			mf = rand.nextInt(2);
			if (mf == 0) {
				sexe = 'F';
			} else {
				sexe = 'M';
			}
			;
			granja.afegir(new Gos(rand.nextInt(21), "Gos " + (i + 1), rand.nextInt(4), sexe, races[rand.nextInt(9)]));

			i++;
		}

		return granja;

	}

	public Gos obtenirGos(int p) {

		if (p >= 0 && p < 100) {
			return this.gossos[p];
		} else {
			return null;
		}
	}
}

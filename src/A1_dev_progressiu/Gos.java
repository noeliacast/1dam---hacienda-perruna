package A1_dev_progressiu;

class Gos {

	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private static int contGossos = 0;
	
	public Gos() {
		//constructor defecte
		sexe = 'M';
		fills = 0;
		nom= "SenseNom";
		edat = 4;
		contGossos ++;
	}
	
	public Gos(int edat, String nom, int fills, char sexe) {
		//constructor rep parametre per aplicar
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		contGossos++;
	}
	
	public Gos(Gos a) {
		//constructor copia
		//crea un altre gos nou per clonar
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;
		contGossos++;
	}
	
	public Gos(String nom) {
		//constructor nom
		this(); //crida al constructor dels mateixos parametres, com no te res al () crida a Gos();
		this.nom = nom;
		
		//al cridar al Gos() ja te el contGossos
	}
	
	public void borda() {
		System.out.println("Guau Guau");
		
	}
	
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setFills(int fills) {
		this.fills = fills;
	}
	
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}
	
	public int getEdat(int edat) {
		return edat;
	}
	
	public String getNom(String nom) {
		return nom;
	}
	
	public int getFills(int fills) {
		return fills;
	}
	
	public char getSexe(char sexe) {
		return sexe;
	}
	
	@Override
	public String toString() {
	
		return "Edat: " + edat + "\nNom: " + nom + "\nFills: "+ fills + "\nSexe: "+ sexe;
		
	}
	
	public void visualitzar() {
		System.out.println(toString());
	}
	
	public void clonar(Gos a) {
		//parametre que iguala a un gos existent
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;
	}
	
	public static int getContGossos() {
		return contGossos;
	}
}

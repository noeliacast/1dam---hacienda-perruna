package A1_dev_progressiu;

public class TestGos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Gos 1: ");
		Gos gos1 = new Gos();
		gos1.visualitzar();
		System.out.println("Contador: " + Gos.getContGossos());
		gos1.borda();
		System.out.println();
	
		System.out.println("Gos 2: ");
		Gos gos2 = new Gos(gos1); //copia
		gos2.visualitzar();
		System.out.println("Contador: " + Gos.getContGossos());
		gos2.borda();
		System.out.println();
		
		System.out.println("Gos 3: ");
		Gos gos3 = new Gos("Mari Carmen");
		gos3.visualitzar();
		System.out.println("Contador: " + Gos.getContGossos());
		gos3.borda();
		System.out.println();
		
		System.out.println("Gos 4: ");
		Gos gos4 = new Gos(10, "Noelia", 80, 'F');
		gos4.visualitzar();
		System.out.println("Contador: " + Gos.getContGossos());
		gos4.borda();
		System.out.println();
	}

}

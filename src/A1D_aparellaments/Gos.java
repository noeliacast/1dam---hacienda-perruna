package A1D_aparellaments;

import java.time.chrono.MinguoDate;
import java.util.Random;

class Gos {

	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private static int contGossos = 0;
	private Raca raca; // objecte
	private GosEstat estat = GosEstat.VIU; // objecte

	public Gos() {
		// constructor defecte
		sexe = 'M';
		fills = 0;
		nom = "SenseNom";
		edat = 4;
		contGossos++;
	}

	public Gos(int edat, String nom, int fills, char sexe) {
		// constructor rep parametre per aplicar
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		if (edat > 10) {
			this.estat = GosEstat.MORT;
		}

		contGossos++;
	}

	public Gos(Gos a) {
		// constructor copia
		// crea un altre gos nou per clonar
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;

		contGossos++;
	}

	public Gos(String nom) {
		// constructor nom
		this(); // crida al constructor dels mateixos parametres, com no te res al () crida a
				// Gos();
		this.nom = nom;

		// al cridar al Gos() ja te el contGossos
	}

	public Gos(int edat, String nom, int fills, char sexe, Raca raca) {
		// constructor ra�a
		this(edat, nom, fills, sexe); // crida a un altre constructor que tingui aquests parametres en el mateix
										// ordre.
		if (edat > raca.getTempsVida()) {
			this.estat = GosEstat.MORT;
		} else {
			this.estat = GosEstat.VIU;
		}
		this.raca = raca; // aplicar la ra�a que cridem
	}

	public void borda() {
		System.out.println("Guau Guau");

	}

	public void setEdat(int edat) {
		if (raca != null && edat > raca.getTempsVida()) {
			estat = GosEstat.MORT;
		} else if (raca == null && edat >= 10) {
			estat = GosEstat.MORT;
		}
		this.edat = edat;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getEdat(int edat) {
		return edat;
	}

	public String getNom(String nom) {
		return nom;
	}

	public int getFills(int fills) {
		return fills;
	}

	public char getSexe(char sexe) {
		return sexe;
	}

	@Override
	public String toString() {

		if (raca != null) {
			return "Nom: " + nom + "\nEdat: " + edat + "\nFills: " + fills + "\nSexe: " + sexe + "\nEstat: " + estat
					+ raca.toString() + "\n";
		} else {
			return "Nom: " + nom + "\nEdat: " + edat + "\nFills: " + fills + "\nSexe: " + sexe + "\nEstat: " + estat
					+ "\n";
		}

	}

	public void visualitzar() {
		System.out.println(toString());
	}

	public void clonar(Gos a) {
		// parametre que iguala a un gos existent
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;
		this.raca = a.raca;
	}

	public static int getContGossos() {
		return contGossos;
	}

	public void setRaca(Raca raca) {
		this.raca = raca; // aplica l'objecte ra�a dins del objecte gos
	}

	public Raca getRaca() {
		return raca; // quan fas un return d'una clasre retorna un toString
	}

	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public Gos aparellar(Gos g) {
		if (this.comprovarAparellar(g)) {
			// TODO aqui deberia estar el crear perro o las caracteristicas aun no se
			// comprovar si te raca
			Random random = new Random();
			char sexe = ' ';
			String nom = " ";
			Gos gfill;

			if (random.nextInt(2) == 0) {
				sexe = 'F';
				if (this.sexe == 'F') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + g.nom;
				}
			} else {
				sexe = 'M';
				if (this.sexe == 'M') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + g.nom;
				}
			}

			Raca racafill = this.calcularRaca(g);
			if (racafill == null) {
				gfill = new Gos(0, nom, 0, sexe);
				
			} else {
				gfill = new Gos(0, nom, 0, sexe, racafill);
				
			}
			this.fills++;
			g.fills++;
			return gfill;
		}
		return null;
	}

	public boolean comprovarAparellar(Gos g) {
		int sexeM = 0;
		int sexeF = 0;
		if (this.estat == GosEstat.VIU && g.estat == GosEstat.VIU) {
			if (edat >= 2 && edat <= 10 && g.edat >= 2 && g.edat <= 10) {
				if (this.sexe != g.sexe) {

					if (this.raca != null && g.raca != null) {

						if (this.sexe == 'F') {
							sexeF = this.tamany();
							sexeM = g.tamany();
						} else {
							sexeM = this.tamany();
							sexeF = g.tamany();
						}

						if (sexeF >= sexeM) {
							if (this.sexe == 'F') {
								if (this.fills < 3) {
									return true;
								}
							} else if (g.sexe == 'F') {
								if (this.fills < 3) {
									return true;
								}
							}
						}
					}
				}

			}
		}
		return false;
	}

	public int tamany() {
		if (this.raca.getMida() == GosMida.PETIT) {
			return 1;
		} else if (this.raca.getMida() == GosMida.MITJA) {
			return 2;
		} else {
			return 3;
		}
	}

	public Raca calcularRaca(Gos g) {
		if (this.raca != null && g.raca != null) {
			if (this.sexe == 'M' && this.raca.getDominant() == true && g.raca.getDominant() == false) {
				raca = this.raca;
				return raca;
			} else if (g.sexe == 'M' && g.raca.getDominant() == true && this.raca.getDominant() == false) {
				raca = g.raca;
				return raca;
			} else if (this.sexe == 'F') {
				raca = this.raca;
				return raca;
			} else if (g.sexe == 'F') {
				raca = g.raca;
				return raca;
			}

		}
		return null;

	}

}

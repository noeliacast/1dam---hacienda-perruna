package C2_gansos_vs_gossos;

import java.util.Random;

public class Granja {

	private Animal[] animals;
	private int numAnimals;
	private int topAnimals;
	private static Raca[] races = { new Raca("Pastor aleman", GosMida.GRAN, 13, true),
			new Raca("Pitbull", GosMida.MITJA, 8, false), new Raca("Caniche", GosMida.PETIT, 10, false),
			new Raca("Dalmata", GosMida.GRAN, 9, true), new Raca("Labrador", GosMida.GRAN, 14, false),
			new Raca("Mastin", GosMida.GRAN, 11, true), new Raca("San bernardo", GosMida.GRAN, 10, true),
			new Raca("Pug", GosMida.PETIT, 15, false), new Raca("Bulldog", GosMida.PETIT, 10, true)

	};

	public Granja() {
		// constructor
		topAnimals = 100;
		numAnimals = 0;
		animals = new Animal[topAnimals];
	}

	public Granja(int top) {
		// constructor modificar top gossos
		numAnimals = 0;
		if (top >= 1 && top < 100) {
			this.topAnimals = top;
		} else {
			topAnimals = 100;
		}
		animals = new Animal[topAnimals];
	}

	public Animal getAnimal(int i) {

		return animals[i];
	}

	public int getnumAnimals() {
		return numAnimals;

	}

	public int gettopAnimals() {
		return topAnimals;
	}

	public int afegir(Animal a) {
		if (a == null) {
			return -1;
		} else {
			if (numAnimals < topAnimals) {
				System.out.println("Posicion del animal: " + numAnimals);
				System.out.println(a.toString());
				animals[numAnimals] = a;
				numAnimals++;
				System.out.println("Ha nascut el "+ a.nom+"!!!");
				return numAnimals;
			} else {
				System.out.println("No hi ha espai pel animal!");
				return -2;
			}
		}

	}

	@Override
	public String toString() {
		int i = 0;
		String string = new String();
		System.out.println();
		while (i < numAnimals) {
			string = string + animals[i].toString();
			i++;
		}

		return string;
	}

public void visualitzar() {
		
		for (int i = 0; i < numAnimals; i++) {
			System.out.println(this.obtenirAnimal(i).toString());
		}
		
	}

	public void visualitzarVius() {
		int i = 0;
		while (i < numAnimals) {
			if (animals[i].getEstat() == AnimalEstat.VIU) {
				System.out.println(animals[i]);
			}
			i++;
		}
	}
	
	static Granja generarGranja(int topAnimal) {
		Random rand = new Random();
		Granja granja = new Granja(topAnimal);
		int i = 0;
		int j = 0;
		int mf = 0;
		int tg = 0;
		char sexe = ' ';
		GansoTipus tipus;
		
		for (i = 0; i < 20; i++) {
			mf = rand.nextInt(2);
			if (mf == 0) {
				sexe = 'F';
			} else {
				sexe = 'M';
			}
			if (rand.nextInt(2) == 0) {
				// gansos
				tg = rand.nextInt(3);
				if (tg == 0) {
					tipus = GansoTipus.AGRESSIU;
				} else if (tg == 1) {
					tipus = GansoTipus.DESCONEGUT;
				} else {
					tipus = GansoTipus.DOMESIC;
				}
				
				Ganso ganso1 = new Ganso("GANSO Numero "+j, 0, tipus, sexe);
				
				j++;
				granja.afegir(ganso1);
			} else {
				// perros
				
				Gos gos1 = new Gos("GOS Numero "+j, 0, races[rand.nextInt(8)], sexe);
				granja.afegir(gos1);
				j++;
			}
		}
		return granja;
	}

	public Animal obtenirAnimal(int p) {

		if (p >= 0 && p <= topAnimals) {
			return this.animals[p];
		} else {
			return null;
		}
	}

	public void canviarPosicio(Animal animal, int i) {

		animals[i] = animal;
	}
	
	public boolean comprovarVius() {
		
		int i = 0;
		
		int gansosCont = 0, gossosCont = 0, mortsCont = 0;
		for (int j = 0; j < getnumAnimals(); j++) {
			if(obtenirAnimal(i) instanceof Gos && obtenirAnimal(i).getEstat() == AnimalEstat.VIU) {
				gossosCont++;
			} else if(obtenirAnimal(i) instanceof Ganso && obtenirAnimal(i).getEstat() == AnimalEstat.VIU){
				gansosCont++;
			} else {
				mortsCont++;
			}
		}
		
		
		if (gansosCont + mortsCont == getnumAnimals()) {
			System.out.println("----------------------\n" + "ELS GANSOS HAN GUANYAT");
			return false;
		}
		else if (gossosCont + mortsCont == getnumAnimals()) {
			System.out.println("----------------------\n" + "ELS GOSSOS HAN GUANYAT");
			return false;
		} else {
			System.out.println("TOTS MORTS");
			return true;
		}
		
	}
	
	
	
}

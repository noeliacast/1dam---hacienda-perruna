package C2_gansos_vs_gossos;

import java.util.Random;

import A1E_evolucio.GosEstat;

public abstract class Animal {

	protected int edat;
	protected String nom;
	protected int fills;
	protected char sexe;
	protected AnimalEstat estat = AnimalEstat.VIU; 

	public Animal() {
		// constructor defecte
		edat = 0;
		nom = "Nombre";
		fills = 0;
		sexe = 'M';
		estat = AnimalEstat.VIU;
	}

	public Animal(int edat, String nom, int fills, char sexe) {
		this.edat = edat;
		this.nom = nom;
		this.fills = 0;
		this.sexe = sexe;
	}

	public Animal(String nom) {
		this.nom = nom;
	}

	public boolean comprovarAparellar(Animal a) {
		int sexeM = 0;
		int sexeF = 0;
		if (this.estat == AnimalEstat.VIU && a.estat == AnimalEstat.VIU) {
			if (this.sexe != a.sexe) {
				return true;
			}
		}
		return false;
	}

	public void visualitzar() {
		System.out.println(toString());
	}

	@Override
	public String toString() {

		return "Nom :" + nom + "\nEdat: " + edat + "\nFills: " + fills + "\nSexe: " + sexe + "\n";

	}

	public abstract void so();

	public abstract Animal aparellar(Animal a);

	public AnimalEstat getEstat() {
		return estat;
	}

	public void pelea(Animal a) {
		
		if (this instanceof Gos) {
			// gossos
			if (((Ganso) a).geTipus() == GansoTipus.AGRESSIU && this.sexe == 'F') {
				this.estat = AnimalEstat.MORT;
				System.out.println(this.nom + " ha mort!");
				a.edat++; 
				a.modificarEstat();
				
			} else if (((Ganso) a).geTipus() == GansoTipus.DOMESIC && ((Gos) this).getRaca() != null
					&& ((Gos) this).getRaca().getDominant()) {
				a.estat = AnimalEstat.MORT;
				System.out.println(a.nom + " ha mort!");
				this.edat++;
				this.modificarEstat();
			} else if (((Ganso) a).geTipus() == GansoTipus.DESCONEGUT && this.sexe == 'F') {
				a.estat = AnimalEstat.MORT;
				System.out.println(a.nom + " ha mort!");
				this.edat++;
				this.modificarEstat();
			}
		} else {
			// gansos
			if (((Ganso) this).geTipus() == GansoTipus.AGRESSIU || a.edat < 2) {
				a.estat = AnimalEstat.MORT;
				System.out.println(a.nom + " ha mort!");
				this.edat++;
				this.modificarEstat();
			} else if (((Ganso) this).geTipus() != GansoTipus.AGRESSIU && a.sexe == 'F'
					&& ((Gos) a).getRaca() != null) {
				this.estat = AnimalEstat.MORT;
				System.out.println(this.nom + " ha mort!");
				a.edat++;
				a.modificarEstat();
			}
		}
	
	}

	public void modificarEstat() {
	
		if (this instanceof Gos) {
			if (this.edat > 10 && ((Gos) this).raca == null && ((Gos) this).estat == AnimalEstat.VIU) {
				this.estat = AnimalEstat.MORT;
			} else if (((Gos) this).raca != null && this.edat > ((Gos) this).raca.getTempsVida() && ((Gos) this).estat == AnimalEstat.VIU) {
				this.estat = AnimalEstat.MORT;
			}
		} else if(this instanceof Ganso && ((Ganso) this).estat == AnimalEstat.VIU) {
			if(this.edat > Ganso.edatTope) {
				this.estat = AnimalEstat.MORT;
		}

	}
}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getFills() {
		return fills;
	}

	public void setFills(int fills) {
		this.fills = fills;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getEdat() {
		return edat;
	}
	
	public void setEdat(int edat) {
		this.edat = edat;
	}

	public void setEstat(AnimalEstat estat) {
		this.estat = estat;
	}
}

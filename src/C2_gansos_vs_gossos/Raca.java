package C2_gansos_vs_gossos;

public class Raca {

	private String nomRaca;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raca(String nom, GosMida gm, int t, boolean d) {
		//constructor temps vida
		this(nom, gm, d);
		this.tempsVida = t;
	}
	
	public Raca(String nom, GosMida gm, boolean d) {
		//constructor general
		this.nomRaca = nom;
		this.mida = gm;
		tempsVida = 10;
		dominant = false;
	}
	
	public void setNomRaca(String n) {
		this.nomRaca = n;
	}
	
	public void setMida(GosMida m) {
		this.mida = m;
	}
	
	public void setTempsVida(int t) {
		this.tempsVida = t;
	}
	
	public void setDominant(boolean d) {
		this.dominant = d;
	}
	
	public String getNomRaca() {
		return nomRaca;
	}
	
	public GosMida getMida() {
		return mida;
	}
	
	public int getTempsVida() {
		return tempsVida;		
	}
	
	public boolean getDominant() {
		return dominant;
	}
	
	@Override
	public String toString() {
		
		return "\nNom: " + nomRaca + "\nMida: " + mida + "\nTemps vida: " + tempsVida + "\nDominant: " + dominant+ "\n";
	}
	
	
}

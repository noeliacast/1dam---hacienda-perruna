package A1E_evolucio;

import java.util.Scanner;

public class TestGos {
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int cicles;
		Granja granja = Granja.generarGranja();
		
		cicles = numCicles();
		controlCicles(granja, cicles);
	}

	
	public static int numCicles() {
		int c=0;
		System.out.println("Ingresa un numero entre 1 i 20: ");
		c = reader.nextInt();
		return c;
	}
	
	public static void controlCicles(Granja gr, int c) {
		Gos aux;
		int j = 0;
		for (int i = 0; i < c; i++) {
		
			if(j>=gr.getnumGossos()-1) {
				j=0;
			}
			System.out.println("Cicle numero: "+i+"\n");
			if(gr.obtenirGos(j).comprovarAparellar(gr.obtenirGos(j+1))) {
			gr.afegir(gr.obtenirGos(j).aparellar(gr.obtenirGos(j+1)));
			System.out.println("Ha nascut un cadell!");
			gr.obtenirGos(j).setEdat(gr.obtenirGos(j).getEdat()+1);
			gr.obtenirGos(j+1).setEdat(gr.obtenirGos(j+1).getEdat()+1);
			j = j+2;
			} else {
			aux = gr.obtenirGos(j);
			gr.canviarPosicio(gr.obtenirGos(j+1), j);
			gr.canviarPosicio(aux, j+1);
			gr.obtenirGos(j).setEdat(gr.obtenirGos(j).getEdat()+1);
			j++;
			}
		gr.visualitzar();
		}
	}
	
	

}

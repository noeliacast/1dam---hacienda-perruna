package A1E_evolucio;

import java.util.Random;

public class Granja {

	private Gos[] gossos;
	private int numGossos;
	private int topGossos;
	private static Raca[] races = { new Raca("Pastor alemany", GosMida.GRAN, 13, true), new Raca("Pitbull", GosMida.MITJA, 8, false),
			new Raca("Caniche", GosMida.PETIT, 10, false), new Raca("Dalmata", GosMida.GRAN, 9, true),
			new Raca("Labrador", GosMida.GRAN, 14, false), new Raca("Mastin", GosMida.GRAN, 11, true),
			new Raca("San bernardo", GosMida.GRAN, 10, true), new Raca("Pug", GosMida.PETIT, 15, false),
			new Raca("Bulldog", GosMida.PETIT, 10, true)

	};

	public Granja() {
		// constructor
		topGossos = 100;
		numGossos = 0;
		gossos = new Gos[topGossos];
	}

	public Granja(int top) {
		// constructor modificar top gossos
		numGossos = 0;
		if (top >= 1 && top <= 100) {
			this.topGossos = top;
		} else {
			topGossos = 100;
		}
		gossos = new Gos[topGossos];
	}

	public Gos[] getGos() {

		return gossos;
	}

	public int getnumGossos() {
		return numGossos;

	}

	public int gettopGossos() {
		return topGossos;
	}

	public int afegir(Gos g) {
		if(g == null) {
			return -1;
		} else {
			if (numGossos < topGossos) {
				gossos[numGossos] = g;
				numGossos++;
				return numGossos;
			} else {
				System.out.println("No hi ha espai pel gos!");
				return -1;
			}
		}
		

	}

	@Override
	public String toString() {
		int i = 0;
		String string = new String();
		System.out.println("Gossos: ");
		System.out.println();
		while (i < numGossos) {
			string = string + gossos[i].toString();
			i++;
		}

		return string;
	}

	public void visualitzar() {

		System.out.println(toString());
	}

	public void visualitzarVius() {
		int i = 0;
		while (i < numGossos) {
			if (gossos[i].getEstat() == GosEstat.VIU) {
				System.out.println(gossos[i]);
			}
			i++;
		}
	}

	static Granja generarGranja() {
		Random rand = new Random();
		Granja granja = new Granja(100);
		int i = 0;
		int mf = 0;
		char sexe = ' ';

		while (i < 10) {
			mf = rand.nextInt(2);
			if (mf == 0) {
				sexe = 'F';
			} else {
				sexe = 'M';
			}
			;
			granja.afegir(new Gos(rand.nextInt(21), "Gos " + (i + 1), rand.nextInt(4), sexe, races[rand.nextInt(9)]));

			i++;
		}

		return granja;

	}

	public Gos obtenirGos(int p) {

		if (p >= 0 && p < 100) {
			return this.gossos[p];
		} else {
			return null;
		}
	}
	
	public void canviarPosicio(Gos g, int i) {
		
		gossos[i]= g;
		
	}
	
}

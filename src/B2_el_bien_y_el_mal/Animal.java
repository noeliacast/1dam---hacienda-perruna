package B2_el_bien_y_el_mal;

import java.util.Random;

import A1E_evolucio.GosEstat;

public abstract class Animal {

	protected int edat;
	protected String nom;
	protected int fills;
	protected char sexe;
	protected AnimalEstat estat = AnimalEstat.VIU; // objecte

	public Animal() {
		// constructor defecte
		edat = 0;
		nom = "Nombre";
		fills = 0;
		sexe = 'M';
	}
	
	public Animal(int edat, String nom, int fills, char sexe) {
		this.edat = edat;
		this.nom = nom;
		this.fills = 0;
		this.sexe = sexe;
	}
	
	public Animal(String nom) {
		this.nom = nom;
	}
	

	public boolean comprovarAparellar(Animal a) {
		int sexeM = 0;
		int sexeF = 0;
		if (this.estat == AnimalEstat.VIU && a.estat == AnimalEstat.VIU) {
				if (this.sexe != a.sexe) {
					return true;
					}
			}
		return false;
}
	
	public void visualitzar() {
		System.out.println(toString());
	}
	
	@Override
	public String toString() {
		
	return "Nom :" + nom + "\nEdat: "+ edat + "\nFills: " + fills + "\nSexe: " + sexe + "\n";
		
	}
	
	public abstract void so();
	
	public abstract Animal aparellar(Animal a);
	
	public AnimalEstat getEstat() {
		return estat;
	}
	
	
	public Animal pelea(Animal a) {
		
		if(this instanceof Gos) {
			//gossos
			if(((Ganso) a).geTipus() == GansoTipus.AGRESSIU && this.sexe == 'F') {
				this.estat = AnimalEstat.MORT;
				return this;
			} else if(((Ganso) a).geTipus() == GansoTipus.DOMESIC && ((Gos) this).getRaca() != null && ((Gos) this).getRaca().getDominant()) {
				a.estat = AnimalEstat.MORT;
				return a;
			} else if(((Ganso) a).geTipus() == GansoTipus.DESCONEGUT && this.sexe == 'F') {
				a.estat = AnimalEstat.MORT;
				return a;
			}
		} else {
			//gansos
			if(((Ganso) this).geTipus() == GansoTipus.AGRESSIU || a.edat < 2) {
				a.estat = AnimalEstat.MORT;
				return a;
			} else if(((Ganso) this).geTipus() != GansoTipus.AGRESSIU && a.sexe == 'F' && ((Gos) a).getRaca() != null) {
				this.estat = AnimalEstat.MORT;
				return this;
			}
		}
		return null;
	}
	
	
	
}

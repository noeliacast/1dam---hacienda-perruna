package B2_el_bien_y_el_mal;

import java.util.Random;

public class Granja {

	private Animal[] animals;
	private int numAnimals;
	private int topAnimals;
	private static Raca[] races = { new Raca("Pastor alemany", GosMida.GRAN, 13, true),
			new Raca("Pitbull", GosMida.MITJA, 8, false), new Raca("Caniche", GosMida.PETIT, 10, false),
			new Raca("Dalmata", GosMida.GRAN, 9, true), new Raca("Labrador", GosMida.GRAN, 14, false),
			new Raca("Mastin", GosMida.GRAN, 11, true), new Raca("San bernardo", GosMida.GRAN, 10, true),
			new Raca("Pug", GosMida.PETIT, 15, false), new Raca("Bulldog", GosMida.PETIT, 10, true)

	};

	public Granja() {
		// constructor
		topAnimals = 20;
		numAnimals = 0;
		animals = new Animal[topAnimals];
	}

	public Granja(int top) {
		// constructor modificar top gossos
		numAnimals = 0;
		if (top >= 1 && top <= 100) {
			this.topAnimals = top;
		} else {
			topAnimals = 100;
		}
		animals = new Animal[topAnimals];
	}

	public Animal getAnimal(int i) {

		return animals[i];
	}

	public int getnumAnimals() {
		return numAnimals;

	}

	public int gettopAnimals() {
		return topAnimals;
	}

	public int afegir(Animal a) {
		if (a == null) {
			return -1;
		} else {
			if (numAnimals < topAnimals) {
				animals[numAnimals] = a;
				numAnimals++;
				return numAnimals;
			} else {
				System.out.println("No hi ha espai pel animal!");
				return -1;
			}
		}

	}

	@Override
	public String toString() {
		int i = 0;
		String string = new String();
		//System.out.println("Gossos: ");
		System.out.println();
		while (i < numAnimals) {
			string = string + animals[i].toString();
			i++;
		}

		return string;
	}

	public void visualitzar() {

		System.out.println(toString());
	}

	public void visualitzarVius() {
		int i = 0;
		while (i < numAnimals) {
			if (animals[i].getEstat() == AnimalEstat.VIU) {
				System.out.println(animals[i]);
			}
			i++;
		}
	}

	static Granja generarGranja(int topAnimal) {
		Random rand = new Random();
		Granja granja = new Granja(topAnimal);
		int i = 0;
		int j = 0;
		int mf = 0;
		int tg = 0;
		char sexe = ' ';
		GansoTipus tipus;
		
		
		for (i = 0; i < topAnimal; i++) {
			mf = rand.nextInt(2);
			if (mf == 0) {
				sexe = 'F';
			} else {
				sexe = 'M';
			}
			if (rand.nextInt(2) == 0) {
				// gansos
				tg = rand.nextInt(3);
				if (tg == 0) {
					tipus = GansoTipus.AGRESSIU;
				} else if (tg == 1) {
					tipus = GansoTipus.DESCONEGUT;
				} else {
					tipus = GansoTipus.DOMESIC;
				}
				
				Ganso ganso1 = new Ganso("GANSO Mari Carmen "+j, 0, tipus, sexe);
				
				j++;
				granja.afegir(ganso1);
			} else {
				// perros
				
				Gos gos1 = new Gos("GOS Aceituno "+j, 0, races[rand.nextInt(8)], sexe);
				granja.afegir(gos1);
				j++;
			}
		}
		return granja;
	}

	public Animal obtenirAnimal(int p) {

		if (p >= 0 && p < 20) {
			return this.animals[p];
		} else {
			return null;
		}
	}

	public void canviarPosicio(Gos g, int i) {

		animals[i] = g;
	}
}

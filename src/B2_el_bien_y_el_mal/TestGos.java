package B2_el_bien_y_el_mal;

public class TestGos {

public static void main(String [] args) {
	int i = 0, j= 0;
	
	Granja gra1 = Granja.generarGranja(20);
	Granja gra2 = Granja.generarGranja(20);
	Granja granjaNous = new Granja();
	Granja granjaMorts = new Granja();
	
	for (i = 0; i < 20; i++) {
		if(gra1.obtenirAnimal(i).getClass().equals(gra2.obtenirAnimal(i).getClass())) {
			granjaNous.afegir(gra1.obtenirAnimal(i).aparellar(gra2.obtenirAnimal(i)));
		} else if(!gra1.obtenirAnimal(i).getClass().equals(gra2.obtenirAnimal(i).getClass())) {
			granjaMorts.afegir(gra1.obtenirAnimal(i).pelea(gra2.obtenirAnimal(i)));
		}
	}
	
	
	
	gra1.visualitzar();
	System.out.println("<=============================================================>");
	gra2.visualitzar();
	System.out.println("<=============================================================>");
	granjaNous.visualitzar();
	if(granjaNous.getnumAnimals() > granjaMorts.getnumAnimals()) {
		System.out.println("L'amor ha triomfat");
	} else if(granjaNous.getnumAnimals() < granjaMorts.getnumAnimals()){
		System.out.println("Kaos");
	} else {
		System.out.println("Hi ha un empat entre emparellaments i morts");
	}
	
		
	
//System.out.println(gos1.toString());
}
}
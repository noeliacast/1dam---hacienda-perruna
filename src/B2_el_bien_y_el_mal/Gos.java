package B2_el_bien_y_el_mal;

import java.util.Random;

class Gos extends Animal{

	private Raca raca; // objecte

	Gos(){
		super();
		raca = null;
	}
	
	Gos(String nom, int edat, Raca raca, char sexe){
		super(edat, nom, 0, sexe);
		this.raca = raca;
	}
	
	Gos(String nom, int edat, char sexe){
		super();
	}
	
	Gos(String nom){
		super(nom);
		raca = null;
	}
	
	Gos(Gos g) {
		this();
		clonar(g);
	}
	
	
	public void clonar(Gos g) {
		this.edat = g.edat;
		this.nom = g.nom;
		this.sexe = g.sexe;
		this.raca = g.raca;
	}
	
	public void visualitzar() {
		System.out.println(toString());
		
	}
	
	@Override
	public String toString() {
		if (raca != null) {
			return "Nom: " + nom + "\nEdat: " + edat + "\nFills: " + fills + "\nSexe: " + sexe + "\nEstat: " + estat + raca.toString() + "\n";
		} else {
			return "Nom: " + nom + "\nEdat: " + edat + "\nFills: " + fills + "\nSexe: " + sexe + "\nEstat: " + estat + "\n";
		}
	}
	public void so() {
		System.out.println("Guau guau!");
	}

	public boolean comprovarAparellar(Animal a) {
		int sexeM = 0;
		int sexeF = 0;
		if(a instanceof Gos) {
		if (this.estat == AnimalEstat.VIU && a.estat == AnimalEstat.VIU) {
				if (this.sexe != a.sexe) {
					return true;
					}
			} 
		} 
		return false;
}

	@Override
	public Animal aparellar(Animal a) {
		if (this.comprovarAparellar(a)) {
			
			Random random = new Random();
			char sexe = ' ';
			String nom = " ";
			Gos afill;

			if (random.nextInt(2) == 0) {
				sexe = 'F';
				if (this.sexe == 'F') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + a.nom;
				}
			} else {
				sexe = 'M';
				if (this.sexe == 'M') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + a.nom;
				}
			}
			
			afill = new Gos(nom, 0, this.raca, sexe);
			this.fills++;
			a.fills++;
			return afill;
		}
		return null;
	}
	
	public Raca getRaca() {
		return raca;
	}
}

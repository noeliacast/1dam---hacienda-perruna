package B1_la_hacienda_de_los_perros_y_gansos;

import java.util.Random;

public class Ganso extends Animal {

	private GansoTipus tipus;
	
	private static int edatTope = 6;

	public Ganso() {
		// TODO Auto-generated constructor stub
		super();
		edatTope=6;
		tipus = GansoTipus.DESCONEGUT;
	}
	
	public Ganso(String nom, int edat, GansoTipus tipus, char sexe){
		super(edat, nom, 0, sexe);
		this.tipus = tipus;
	}
	
	public Ganso(String n){
		
	}
	
	Ganso(Ganso g){
		this();
		clonar();
	}
	
	public void clonar() {
		this.edat = edat;
		this.nom = nom;
		this.sexe = sexe;
	}
	
	public void visualitzar() {
		System.out.println(toString());
	}
	
	@Override
	public String toString() {
		
	return "Nom: " + nom + "\nEdat: " + edat + "\nTipus: " + tipus + "\nSexe: " + sexe + "\n";
		
	}

	@Override
	public void so() {
		// TODO Auto-generated method stub
		System.out.println("Ganso ganso!");
	}

	@Override
	public Animal aparellar(Animal a) {
		if (this.comprovarAparellar(a)) {
		
			Random random = new Random();
			char sexe = ' ';
			String nom = " ";
			Ganso afill;

			if (random.nextInt(2) == 0) {
				sexe = 'F';
				if (this.sexe == 'F') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + a.nom;
				}
			} else {
				sexe = 'M';
				if (this.sexe == 'M') {
					nom = "Fill de " + this.nom;
				} else {
					nom = "Fill de " + a.nom;
				}
			}
			
			afill = new Ganso(nom, 0, GansoTipus.DESCONEGUT, sexe);
			this.fills++;
			a.fills++;
			return afill;
		}
		return null;
	}

	public boolean comprovarAparellar(Animal a) {
		int sexeM = 0;
		int sexeF = 0;
		if (a instanceof Ganso) {
		if (this.estat == AnimalEstat.VIU && a.estat == AnimalEstat.VIU) {
				if (this.sexe != a.sexe) {
					return true;
					}
			}
		}
		return false;
}
}

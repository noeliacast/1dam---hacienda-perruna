package B1_la_hacienda_de_los_perros_y_gansos;

import java.util.Random;

import A1E_evolucio.GosEstat;

public abstract class Animal {

	protected int edat;
	protected String nom;
	protected int fills;
	protected char sexe;
	protected AnimalEstat estat = AnimalEstat.VIU; // objecte

	public Animal() {
		// constructor defecte
		edat = 0;
		nom = "Nombre";
		fills = 0;
		sexe = 'M';
		estat = AnimalEstat.VIU;
	}
	
	public Animal(int edat, String nom, int fills, char sexe) {
		this.edat = edat;
		this.nom = nom;
		this.fills = 0;
		this.sexe = sexe;
	}
	
	public Animal(String nom) {
		this.nom = nom;
	}
	

	public boolean comprovarAparellar(Animal a) {
		int sexeM = 0;
		int sexeF = 0;
		if (this.estat == AnimalEstat.VIU && a.estat == AnimalEstat.VIU) {
				if (this.sexe != a.sexe) {
					return true;
					}
			}
		return false;
}
	
	public void visualitzar() {
		System.out.println(toString());
	}
	
	@Override
	public String toString() {
		
	return "Nom :" + nom + "\nEdat: "+ edat + "\nFills: " + fills + "\nSexe: " + sexe + "\n";
		
	}
	
	public abstract void so();
	
	public abstract Animal aparellar(Animal a);
	
	public AnimalEstat getEstat() {
		return estat;
	}
	
	
}

package A1B_les_races;

class Gos {

	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private static int contGossos = 0;
	private Raca raca; //objecte
	private GosEstat estat = GosEstat.VIU; //objecte
	
	public Gos() {
		//constructor defecte
		sexe = 'M';
		fills = 0;
		nom= "SenseNom";
		edat = 4;
		contGossos ++;
	}
	
	public Gos(int edat, String nom, int fills, char sexe) {
		//constructor rep parametre per aplicar
		this.edat = edat;
		this.nom = nom;
		this.fills = fills;
		this.sexe = sexe;
		contGossos++;
	}
	
	public Gos(Gos a) {
		//constructor copia
		//crea un altre gos nou per clonar
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;
		contGossos++;
	}
	
	public Gos(String nom) {
		//constructor nom
		this(); //crida al constructor dels mateixos parametres, com no te res al () crida a Gos();
		this.nom = nom;
		
		//al cridar al Gos() ja te el contGossos
	}
	
	public Gos(int edat, String nom, int fills, char sexe, Raca raca) {
		//constructor ra�a
		this(edat,nom,fills,sexe); //crida a un altre constructor que tingui aquests parametres en el mateix ordre.
		this.raca = raca; //aplicar la ra�a que cridem 
	}
	
	public void borda() {
		System.out.println("Guau Guau");
		
	}
	
	public void setEdat(int edat) {
		if(raca != null && edat > raca.getTempsVida()) {
			estat = GosEstat.MORT;
		} else if(raca == null && edat >= 10) {
			estat = GosEstat.MORT;
		}
		this.edat = edat;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setFills(int fills) {
		this.fills = fills;
	}
	
	public void setSexe(char sexe) {
		this.sexe = sexe;
	}
	
	public int getEdat(int edat) {
		return edat;
	}
	
	public String getNom(String nom) {
		return nom;
	}
	
	public int getFills(int fills) {
		return fills;
	}
	
	public char getSexe(char sexe) {
		return sexe;
	}
	
	@Override
	public String toString() {
	
		
		if(raca != null) {
			return "Edat: " + edat + "\nNom: " + nom + "\nFills: "+ fills + "\nSexe: "+ sexe + "\nEstat: " + estat + raca.toString();
		} else {
			return "Edat: " + edat + "\nNom: " + nom + "\nFills: "+ fills + "\nSexe: "+ sexe + "\nEstat: " + estat;
		}
		
	}
	
	public void visualitzar() {
		System.out.println(toString());
	}
	
	public void clonar(Gos a) {
		//parametre que iguala a un gos existent
		this.edat = a.edat;
		this.nom = a.nom;
		this.fills = a.fills;
		this.sexe = a.sexe;
		this.raca = a.raca;
	}
	
	public static int getContGossos() {
		return contGossos;
	}
	
	public void setRaca(Raca raca) {
		this.raca = raca; //aplica l'objecte ra�a dins del objecte gos
	}
	
	public Raca getRaca() {
		return raca; //quan fas un return d'una clasre retorna un toString
	}
	
	public void setEstat(GosEstat estat) {
		this.estat = estat;
	}
	
	public GosEstat getEstat() {
		return estat;
	}
}

package A1B_les_races;

public class TestGos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Raca pastor = new Raca("Pastor Alemany", GosMida.GRAN);
		Raca beagle = new Raca("Beagle", GosMida.MITJA, 15);
		Gos gos1 = new Gos(9, "Gines", 0, 'M', pastor);
		gos1.setEdat(11);
		gos1.visualitzar();
		System.out.println();
		
		Gos gos2 = new Gos(gos1);
		gos2.setRaca(beagle);
		gos2.visualitzar();
		System.out.println();
		
		
		Gos gos3 = new Gos();
		gos3.clonar(gos1);
		gos3.visualitzar();
	}

}
